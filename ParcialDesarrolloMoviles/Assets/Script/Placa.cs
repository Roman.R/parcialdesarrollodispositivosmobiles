using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Placa : MonoBehaviour
{
    public GameObject door;          // La puerta que queremos controlar
    public float openDuration = 3f;  // Duraci�n que la puerta permanece abierta
    public float cooldown = 12f;     // Tiempo que la placa de presi�n tarda en poder ser activada nuevamente

    private bool isActivated = false;
    private bool isInCooldown = false;

    private void OnTriggerEnter(Collider other)
    {
        // Verifica si la placa no est� en cooldown y no est� actualmente activada
        if (!isInCooldown && !isActivated)
        {
            StartCoroutine(OpenDoor());
        }
    }

    private IEnumerator OpenDoor()
    {
        // Activa la placa y abre la puerta
        isActivated = true;
        door.SetActive(false);  // Suponiendo que desactivamos la puerta para abrirla, ajusta esto seg�n tu l�gica

        // Espera la duraci�n de apertura
        yield return new WaitForSeconds(openDuration);

        // Cierra la puerta
        door.SetActive(true);   // Reactivamos la puerta para cerrarla

        // Inicia el cooldown
        isActivated = false;
        isInCooldown = true;

        // Espera el tiempo de cooldown
        yield return new WaitForSeconds(cooldown);

        // Finaliza el cooldown
        isInCooldown = false;
    }
}
