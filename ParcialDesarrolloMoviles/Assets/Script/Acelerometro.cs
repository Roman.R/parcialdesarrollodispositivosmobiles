using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Acelerometro : MonoBehaviour
{
    public float speed = 10f; // Velocidad de movimiento
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        //Esto permite el movimiento desde el movil
        Vector3 movement = new Vector3(Input.acceleration.x, 0.0f, Input.acceleration.y);
        rb.AddForce(movement * speed);
    }
}
