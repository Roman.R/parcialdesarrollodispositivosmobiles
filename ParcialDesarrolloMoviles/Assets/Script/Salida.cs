using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Salida : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        // Verifica si el objeto que colisiona es el jugador (u otro objeto espec�fico)
        if (other.CompareTag("Player"))
        {
            EndGame();
        }
    }

    private void EndGame()
    {
        // Aqu� puedes poner la l�gica para finalizar el juego, por ejemplo, cargar una escena de Game Over
        // SceneManager.LoadScene("GameOverScene");

        // Alternativamente, puedes simplemente salir del juego
        Application.Quit();

        // Si est�s en el editor de Unity, usa este comando para parar el juego
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
    }
}
